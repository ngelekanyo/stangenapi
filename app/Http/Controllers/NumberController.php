<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NumberController extends Controller
{  
    public function ShowMultiples(Request $request)
    {
        //Check  if provied data is invalid
        $isValid =  $this->isInputDataValid($request);

        //If input data is invalid then return an error message
        if(!$isValid){
            return response()->json([
                'success' => false,
                'message' => "Invalid data",
                'data' => null,
            ]);
        }

        //Get posted data from client app
        $startIndex = $request->startIndex;
        $endIndex = $request->endIndex;

        $results = array();

        for($startIndex; $startIndex <= $endIndex; $startIndex++)
        {
            if($startIndex % 3 == 0 && $startIndex % 5 == 0)
            {
                //Add multiple of 3 and 5
                array_push($results,"Stangen");
            }
            else if($startIndex % 3 == 0)
            {
                //Add multiple of 3
                array_push($results,"Stan");
            }
            else if($startIndex % 5 == 0)
            {
                //Add multiple of 3
                array_push($results,"Gen");
            }
            else{
                //Add number if it's not a multiple of 3 or 5
                array_push($results, $startIndex);
            }
        }

        //Return results
         return response()->json([
            'success' => true,
            'message' => "Procced Successfully",
            'data' => $results,
        ], 200);
    }

    public function isInputDataValid(Request $request)
    {
        $startIndex = $request->startIndex;
        $endIndex = $request->endIndex;

        if($request == null){
            return false;
        }

        if(is_numeric($startIndex)){
            if($startIndex < 0){
                return false;
            }
        }else{
            return false;
        }

        if(is_numeric($endIndex)){
            if($endIndex < 0){
                return false;
            }
        }else{
            return false;
        }

        if($startIndex > $endIndex)
        {
            return false;
        }
        return true; 
    }
}
